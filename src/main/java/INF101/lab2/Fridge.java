package INF101.lab2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    List<FridgeItem> items = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()){
            items.add(item);

            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.contains(item)) {
            items.remove(item);
        }
        else {
            throw new NoSuchElementException("Finnes ikke i kjøleskapet!");
        }

    }

    @Override
    public void emptyFridge() {
        items.clear();


    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        Iterator<FridgeItem> iterator = items.iterator();
        while(iterator.hasNext()) {
            FridgeItem item = iterator.next();
            if (item.hasExpired()) {
                iterator.remove();
                expiredItems.add(item);


            }
        }

        return expiredItems;
    }
}
